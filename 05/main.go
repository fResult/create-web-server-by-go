package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

func main() {
	// Listen ที่ localhost:8080
	li, err := net.Listen("tcp", ":8080")

	// ถ้ามี Error ให้จบ Program
	if err != nil {
		panic(err)
	}

	// เพิ่ม Infinite for loop เพื่อให้สามารถรอรับ Request ถัดไปได้
	for {
		// Code หยุดรอที่บรรทัดนี้จนกว่าจะมี Client request เข้ามา
		conn, err := li.Accept()

		// ถ้ามี Error ให้จบ Program
		if err != nil {
			// เปลี่ยนเป็น log error ออกมาแทน เนื่องจากถ้าใช้ panic แล้ว web server จะหยุดทำงานทันที
			// ซึ่งจะกระทบกับผู้ใช้งานทั้งหมด แม้ว่าจะ error แค่ request เดียว
			log.Println(err)
			continue
		}

		// แตก goroutine (คล้าย thread ในภาษาอื่น) เพื่อให้สามารถทำงานแบบ concurrent ได้
		// ทำให้สามารถรองรับ user หลาย request พร้อมๆ กันได้
		go func(conn net.Conn) {
			// Scan text จาก Client
			scanner := bufio.NewScanner(conn)

			// ถ้าเจอการขึ้นบรรทัดใหม่ ให้พิมพ์ text ออกทาง console แล้ว loop ขึ้นมา scan ใหม่
			for scanner.Scan() {
				ln := scanner.Text()
				fmt.Println("Text from client's request: " + ln)
			}

			// Close connection เมื่อทำงานเสร็จเรียบร้อย
			conn.Close()
		}(conn) // ส่ง connection เข้าไปที่ function พร้อมกับ invoke (IIFE)
	}
}
