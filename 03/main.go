package main

import (
	"io"
	"log"
	"net"
)

func main() {
	// Listen ที่ localhost:8080
	li, err := net.Listen("tcp", ":8080")

	// ถ้ามี Error ให้จบ Program
	if err != nil {
		panic(err)
	}

	// เพิ่ม Infinite for loop เพื่อให้สามารถรอรับ Request ถัดไปได้
	for {
		// Code หยุดรอที่บรรทัดนี้จนกว่าจะมี Client request เข้ามา
		conn, err := li.Accept()

		// ถ้ามี Error ให้จบ Program
		if err != nil {
			// เปลี่ยนเป็น log error ออกมาแทน เนื่องจากถ้าใช้ panic แล้ว web server จะหยุดทำงานทันที
			// ซึ่งจะกระทบกับผู้ใช้งานทั้งหมด แม้ว่าจะ error แค่ request เดียว
			log.Println(err)
			continue
		}

		// Write string กลับไปที่ connection ที่ request เข้ามา
		io.WriteString(conn, "This message from TCP server.")

		// Close connection เมื่อทำงานเสร็จเรียบร้อย
		conn.Close()
	}
}
