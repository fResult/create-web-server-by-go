package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"
)

func main() {
	// Listen ที่ localhost:8080
	li, err := net.Listen("tcp", ":8080")

	// ถ้ามี Error ให้จบ Program
	if err != nil {
		panic(err)
	}

	// เพิ่ม Infinite for loop เพื่อให้สามารถรอรับ Request ถัดไปได้
	for {
		// Code หยุดรอที่บรรทัดนี้จนกว่าจะมี Client request เข้ามา
		conn, err := li.Accept()

		// ถ้ามี Error ให้จบ Program
		if err != nil {
			// เปลี่ยนเป็น log error ออกมาแทน เนื่องจากถ้าใช้ panic แล้ว web server จะหยุดทำงานทันที
			// ซึ่งจะกระทบกับผู้ใช้งานทั้งหมด แม้ว่าจะ error แค่ request เดียว
			log.Println(err)
			continue
		}

		// แตก goroutine (คล้าย thread ในภาษาอื่น) เพื่อให้สามารถทำงานแบบ concurrent ได้
		// ทำให้สามารถรองรับ user หลาย request พร้อมๆ กันได้
		go func(conn net.Conn) {
			// ประกาศตัวแปรเพื่อเก็บ content ในแต่ละ route
			var content string

			//ประกาศตัวแปร i เพื่อเก็บค่าว่ากำลัง scan บรรทัดไหนอยู่
			i := 0

			// Scan text จาก Client
			scanner := bufio.NewScanner(conn)

			// ถ้าเจอการขึ้นบรรทัดใหม่ ให้พิมพ์ text ออกทาง console แล้ว loop ขึ้นมา scan ใหม่
			for scanner.Scan() {
				ln := scanner.Text()
				fmt.Println(ln)

				// ถ้าเจอบรรทัดไหนเป็นบรรทัดว่างๆ ให้ถือว่าหมด header และออกจาก loop เพื่อหยุด scan
				if i == 0 {
					// คล้ายๆ กับการ split ด้วยช่องว่าง
					words := strings.Fields(ln)

					// เก็บค่า method และ uri เข้าตัวแปร เพื่อนำไปใช้กับ router
					method := words[0]
					uri := words[1]

					// router
					if method == "GET" && uri == "/" {
						content = "Hello World"
					} else if method == "GET" && uri == "/page1" {
						content = "Page 1"
					} else if method == "GET" && uri == "/page2" {
						content = "Page 2"
					}

					// Method สามารถเป็นอะไรก็ได้ แต่ควรจะทำตาม convention
					// สามารถทดลองยิงด้วย nc ได้
					if method == "WHATEVER" && uri == "/" {
						content = "This is easter egg"
					}
				}

				if ln == "" {
					break
				}

				i++
			}

			// Return HTML response to client
			body := fmt.Sprintf(`<!Doctype html>
				<html lang="en">
					<head>
						<meta charset="UTF-8">
							<title></title>
					</head>
					<body>
						<strong>%s</strong>
					</body>
				</html>`, content)
			fmt.Fprint(conn, "HTTP/1.1 200 OK\r\n")
			fmt.Fprintf(conn, "Content-Length: %d\r\n", len(body))
			fmt.Fprint(conn, "Content-Type: text/html; charset=utf-8\r\n")
			fmt.Fprint(conn, "\r\n")
			fmt.Fprint(conn, body)

			// Close connection เมื่อทำงานเสร็จเรียบร้อย
			conn.Close()
		}(conn) // ส่ง connection เข้าไปที่ function พร้อมกับ invoke (IIFE)
	}
}
