package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
)

func main() {
	// Listen ที่ localhost:8080
	li, err := net.Listen("tcp", ":8080")

	// ถ้ามี Error ให้จบ Program
	if err != nil {
		panic(err)
	}

	// เพิ่ม Infinite for loop เพื่อให้สามารถรอรับ Request ถัดไปได้
	for {
		// Code หยุดรอที่บรรทัดนี้จนกว่าจะมี Client request เข้ามา
		conn, err := li.Accept()

		// ถ้ามี Error ให้จบ Program
		if err != nil {
			// เปลี่ยนเป็น log error ออกมาแทน เนื่องจากถ้าใช้ panic แล้ว web server จะหยุดทำงานทันที
			// ซึ่งจะกระทบกับผู้ใช้งานทั้งหมด แม้ว่าจะ error แค่ request เดียว
			log.Println(err)
			continue
		}

		// แตก goroutine (คล้าย thread ในภาษาอื่น) เพื่อให้สามารถทำงานแบบ concurrent ได้
		// ทำให้สามารถรองรับ user หลาย request พร้อมๆ กันได้
		go func(conn net.Conn) {
			// ประกาศตัวแปร method และ uri ไว้นอก for loop
			var method string
			var uri string

			//ประกาศตัวแปร i เพื่อเก็บค่าว่ากำลัง scan บรรทัดไหนอยู่
			i := 0

			// Scan text จาก Client
			scanner := bufio.NewScanner(conn)

			// ถ้าเจอการขึ้นบรรทัดใหม่ ให้พิมพ์ text ออกทาง console แล้ว loop ขึ้นมา scan ใหม่
			for scanner.Scan() {
				ln := scanner.Text()
				fmt.Println(ln)

				// ถ้าเจอบรรทัดไหนเป็นบรรทัดว่างๆ ให้ถือว่าหมด header และออกจาก loop เพื่อหยุด scan
				if i == 0 {
					// คล้ายๆ กับการ split ด้วยช่องว่าง
					words := strings.Fields(ln)

					// เก็บค่า method และ uri เข้าตัวแปร เพื่อนำไปใช้กับ router
					method = words[0]
					uri = words[1]

				}

				if ln == "" {
					break
				}

				i++
			}

			if method == "GET" && uri == "/cat.jpg" {
				fmt.Fprint(conn, "HTTP/1.1 200 OK\r\n")

				// ใส่ header เพื่อบอกให้ browser รู้ว่า นี่คือ image
				fmt.Fprint(conn, "Content-Type: image/jpeg\r\n\r\n")

				// เปิดไฟล์ cat.jpg
				f, err := os.Open("./cat.jpg")
				if err != nil {
					log.Println(err)
				}

				// เอา content ในไฟล์เขียนลงไปใน Connection
				io.Copy(conn, f)

				// Close file เพื่อป้องกัน memory leak
				f.Close()
			}

			if method == "GET" && uri == "/" {
				fmt.Fprint(conn, "HTTP/1.1 200 OK\r\n")
				fmt.Fprint(conn, "Content-Type: text/html\r\n\r\n")
				fmt.Fprint(conn, `<img src="/cat.jpg">`)
			}

			// Close connection เมื่อทำงานเสร็จเรียบร้อย
			conn.Close()
		}(conn) // ส่ง connection เข้าไปที่ function พร้อมกับ invoke (IIFE)
	}
}
