package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

func main() {
	// Listen ที่ localhost:8080
	li, err := net.Listen("tcp", ":8080")

	// ถ้ามี Error ให้จบ Program
	if err != nil {
		panic(err)
	}

	// เพิ่ม Infinite for loop เพื่อให้สามารถรอรับ Request ถัดไปได้
	for {
		// Code หยุดรอที่บรรทัดนี้จนกว่าจะมี Client request เข้ามา
		conn, err := li.Accept()

		// ถ้ามี Error ให้จบ Program
		if err != nil {
			// เปลี่ยนเป็น log error ออกมาแทน เนื่องจากถ้าใช้ panic แล้ว web server จะหยุดทำงานทันที
			// ซึ่งจะกระทบกับผู้ใช้งานทั้งหมด แม้ว่าจะ error แค่ request เดียว
			log.Println(err)
			continue
		}

		// แตก goroutine (คล้าย thread ในภาษาอื่น) เพื่อให้สามารถทำงานแบบ concurrent ได้
		// ทำให้สามารถรองรับ user หลาย request พร้อมๆ กันได้
		go func(conn net.Conn) {
			// Scan text จาก Client
			scanner := bufio.NewScanner(conn)

			// ถ้าเจอการขึ้นบรรทัดใหม่ ให้พิมพ์ text ออกทาง console แล้ว loop ขึ้นมา scan ใหม่
			for scanner.Scan() {
				ln := scanner.Text()
				fmt.Println(ln)

				// ถ้าเจอบรรทัดไหนเป็นบรรทัดว่างๆ ให้ถือว่าหมด header และออกจาก loop เพื่อหยุด scan
				if ln == "" {
					break
				}
			}

			// Return HTML response to client
			// body := `<!Doctype html><html lang="en"><head><meta charset="UTF-8"><title></title></head><body><strong>Hello, world.</strong></body></html>`
			// body := string([]byte{224, 184, 151, 224, 184, 148, 224, 184, 170, 224, 184, 173, 224, 184, 154, 224, 184, 160, 224, 184, 178, 224, 184, 169, 224, 184, 178, 224, 185, 132, 224, 184, 151, 224, 184, 162})
			// tis-620 byte slice
			body := string([]byte{183, 180, 202, 205, 186, 192, 210, 201, 210, 228, 183, 194, 32})
			fmt.Fprint(conn, "HTTP/1.1 200 OK\r\n")
			fmt.Fprintf(conn, "Content-Length: %d\r\n", len(body))
			// fmt.Fprint(conn, "Content-Type: text/plain; charset=utf-8\r\n")
			fmt.Fprint(conn, "Content-Type: text/plain; charset=tis-620\r\n")
			fmt.Fprint(conn, "\r\n")
			fmt.Fprint(conn, body)

			// Close connection เมื่อทำงานเสร็จเรียบร้อย
			conn.Close()
		}(conn) // ส่ง connection เข้าไปที่ function พร้อมกับ invoke (IIFE)
	}
}
